using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using Serialize.Linq.Extensions;
using Dominus.Backend.Data;
using Dominus.Backend.DataBase;

namespace Blazor.Infrastructure.Entities
{
    /// <summary>
    /// Archivos object for mapped table Archivos.
    /// </summary>
    [Table("Archivos")]
    public partial class Archivos : BaseEntity
    {

       #region Columnas normales)

       [Column("Descripcion")]
       [DDisplayName("Archivos.Descripcion")]
       [DStringLength("Archivos.Descripcion",1000)]
       public virtual String Descripcion { get; set; }

       [Column("Nombre")]
       [DDisplayName("Archivos.Nombre")]
       [DRequired("Archivos.Nombre")]
       [DStringLength("Archivos.Nombre",1000)]
       public virtual String Nombre { get; set; }

       [Column("TipoContenido")]
       [DDisplayName("Archivos.TipoContenido")]
       [DRequired("Archivos.TipoContenido")]
       [DStringLength("Archivos.TipoContenido",100)]
       public virtual String TipoContenido { get; set; }

       [Column("Extension")]
       [DDisplayName("Archivos.Extension")]
       [DRequired("Archivos.Extension")]
       [DStringLength("Archivos.Extension",10)]
       public virtual String Extension { get; set; }

       [Column("Archivo")]
       [DDisplayName("Archivos.Archivo")]
       [DRequired("Archivos.Archivo")]
       public virtual Byte[] Archivo { get; set; }

       [Column("PesoEnBytes")]
       [DDisplayName("Archivos.PesoEnBytes")]
       [DRequired("Archivos.PesoEnBytes")]
       public virtual Int64 PesoEnBytes { get; set; }

       [Column("PesoResumen")]
       [DDisplayName("Archivos.PesoResumen")]
       [DRequired("Archivos.PesoResumen")]
       [DStringLength("Archivos.PesoResumen",20)]
       public virtual String PesoResumen { get; set; }

       #endregion

       #region Columnas referenciales)

       [Column("PeriodoId")]
       [DDisplayName("Archivos.PeriodoId")]
       [DRequired("Archivos.PeriodoId")]
       [DRequiredFK("Archivos.PeriodoId")]
       public virtual Int64 PeriodoId { get; set; }

       #endregion

       #region Propiedades referencias de entrada)

       [ForeignKey("PeriodoId")]
       public virtual Periodos Periodo { get; set; }

       #endregion

       #region Reglas expression

       public override Expression<Func<T, bool>> PrimaryKeyExpression<T>()
       {
       Expression<Func<Archivos, bool>> expression = entity => entity.Id == this.Id;
       return expression as Expression<Func<T, bool>>;
       }

       public override List<ExpRecurso> GetAdicionarExpression<T>()
       {
        var rules = new List<ExpRecurso>();
        Expression<Func<Archivos, bool>> expression = null;

       return rules;
       }

       public override List<ExpRecurso> GetModificarExpression<T>()
       {
        var rules = new List<ExpRecurso>();
        Expression<Func<Archivos, bool>> expression = null;

       return rules;
       }

       public override List<ExpRecurso> GetEliminarExpression<T>()
       {
        var rules = new List<ExpRecurso>();
       return rules;
       }

       #endregion
    }
 }
