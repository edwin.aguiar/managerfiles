using Blazor.Infrastructure.Entities;
using Dominus.Backend.Data;
using System.Collections.Generic;

namespace Blazor.WebApp.Models
{
    public partial class CargaArchivosModel
    {
        public List<Periodos> Periodos = new List<Periodos>();
        [DDisplayName("CargaArchivos.Periodo")]
        public long PeriodoId { get; set; }
        [DDisplayName("CargaArchivos.Descripcion")]
        public string Descripcion { get; set; }
    }
}
