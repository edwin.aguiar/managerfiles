--Script Creacion Manejador Archivos

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
	
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](255) NOT NULL,
	[Email] [varchar](255) NOT NULL,
	[Names] [varchar](60) NULL,
	[LastNames] [varchar](60) NULL,
	[IdentificationNumber] [varchar](20) NULL,
	[IdentificationTypeId] [varchar](3) NULL,
	[GenderId] [varchar](1) NULL,
	[IsActive] [bit] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_Users] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsActive]  DEFAULT ((0)) FOR [IsActive]
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Id'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Nombre usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserName'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Contraseña' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Password'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contraseña' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Password'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Correo electronico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Email'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Correo electronico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Email'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Nombres' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Names'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Apellidos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'LastNames'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Nro Identificación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'IdentificationNumber'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id tipo identificación' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'IdentificationTypeId'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id genero' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'GenderId'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Activo?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'IsActive'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ultimo usuario que actualizo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado el' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Usuarios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuarios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users'
GO


CREATE TABLE [dbo].[Menus](
	[Id] [bigint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[ResourceKey] [varchar](255) NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Master] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_Menus] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MenuActions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionName] [varchar](50) NOT NULL,
	[ResourceKey] [varchar](255) NOT NULL,
	[MenuId] [bigint] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Methods] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[Profiles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[SecurityPolicy] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Profiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'Id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador unico del registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'Id'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Descripción' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripcion del perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 Deny all 1 Allow All' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'SecurityPolicy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Activo?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'IsActive'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ultimo usuario que actualizo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado el' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Perfiles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Perfiles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Profiles'
GO


CREATE TABLE [dbo].[ProfileUsers](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProfileId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ProfileUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProfileUsers]  WITH CHECK ADD  CONSTRAINT [FK_ProfileUsers_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO

ALTER TABLE [dbo].[ProfileUsers] CHECK CONSTRAINT [FK_ProfileUsers_Profiles]
GO

ALTER TABLE [dbo].[ProfileUsers]  WITH CHECK ADD  CONSTRAINT [FK_ProfileUsers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO

ALTER TABLE [dbo].[ProfileUsers] CHECK CONSTRAINT [FK_ProfileUsers_Users]
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'Id'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'ProfileId'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'UserId'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ultimo usuario que actualizo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado el' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Usuarios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuarios por perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfileUsers'
GO


CREATE TABLE [dbo].[ProfileNavigation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProfileId] [bigint] NOT NULL,
	[Path] [varchar](255) NOT NULL,
	[MenuId] [bigint] NOT NULL,
	[MenuActionId] [bigint] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ProfilesNavigation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProfileNavigation]  WITH CHECK ADD  CONSTRAINT [FK_ProfileNavigation_Masters] FOREIGN KEY([MenuId])
REFERENCES [dbo].[Menus] ([Id])
GO

ALTER TABLE [dbo].[ProfileNavigation] CHECK CONSTRAINT [FK_ProfileNavigation_Masters]
GO

ALTER TABLE [dbo].[ProfileNavigation]  WITH CHECK ADD  CONSTRAINT [FK_ProfileNavigation_Methods] FOREIGN KEY([MenuActionId])
REFERENCES [dbo].[MenuActions] ([Id])
GO

ALTER TABLE [dbo].[ProfileNavigation] CHECK CONSTRAINT [FK_ProfileNavigation_Methods]
GO

ALTER TABLE [dbo].[ProfileNavigation]  WITH CHECK ADD  CONSTRAINT [FK_ProfileNavigation_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([Id])
GO

ALTER TABLE [dbo].[ProfileNavigation] CHECK CONSTRAINT [FK_ProfileNavigation_Profiles]
GO

CREATE TABLE [dbo].[Archivos](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PeriodoId] [bigint] NOT NULL,
	[Descripcion] [varchar](1000) NULL,
	[Nombre] [varchar](1000) NOT NULL,
	[TipoContenido] [varchar](100) NOT NULL,
	[Extension] [varchar](10) NOT NULL,
	[Archivo] [varbinary](max) NOT NULL,
	[PesoEnBytes] [bigint] NOT NULL,
	[PesoResumen] [varchar](20) NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Archivos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Empresas](
	[Id] [bigint] NOT NULL,
	[RazonSocial] [varchar](150) NOT NULL,
	[NumeroIdentificacion] [varchar](45) NOT NULL,
	[DV] [varchar](2) NOT NULL,
	[Direccion] [varchar](200) NULL,
	[Telefono] [varchar](10) NULL,
	[Celular] [varchar](10) NULL,
	[CorreoElectronico] [varchar](200) NULL,
	[PaginaWeb] [varchar](100) NULL,
	[TiposIdentificacionRepresentanteLegalId] [bigint] NULL,
	[NumeroIdentificacionRepresentanteLegal] [varchar](45) NULL,
	[NombresApellidosRepresentanteLegal] [varchar](100) NULL,
	[CIIU] [varchar](20) NULL,
	[CodigoPostal] [varchar](20) NULL,
	[Logo] [varbinary](max) NULL,
	[Alias] [varchar](50) NOT NULL,
	[CodigoReps] [varchar](45) NULL,
	[Codigo85] [varchar](2) NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UQ_alias] UNIQUE NONCLUSTERED 
(
	[Alias] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[Languages](
	[Id] [bigint] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Culture] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'Id'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Código' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'Code'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Idioma' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'Culture'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Nombre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'Name'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Activo?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'Active'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ultimo usuario que actualizo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado el' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Idiomas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Idiomas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Languages'
GO


CREATE TABLE [dbo].[LanguageResources](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[ResourceKey] [varchar](255) NOT NULL,
	[ResourceValue] [varchar](1024) NOT NULL,
	[Active] [bit] NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_LanguageResources] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UQ_LanguageResources_LanguageId_ResourceKey] UNIQUE NONCLUSTERED 
(
	[LanguageId] ASC,
	[ResourceKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LanguageResources]  WITH CHECK ADD  CONSTRAINT [FK_LanguageResources_Languages] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
GO

ALTER TABLE [dbo].[LanguageResources] CHECK CONSTRAINT [FK_LanguageResources_Languages]
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'Id'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Idioma' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'LanguageId'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Llave' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'ResourceKey'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Valor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'ResourceValue'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Activo?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'Active'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ultimo usuario que actualizo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Actualizado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado por' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Creado el' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO

EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Recursos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Recursos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LanguageResources'
GO


INSERT INTO [dbo].[Profiles]
           ([Description]
           ,[SecurityPolicy]
           ,[IsActive]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           ('SUPERCEO'
           ,1
           ,1
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[Users]
           ([UserName]
           ,[Password]
           ,[Email]
           ,[Names]
           ,[LastNames]
           ,[IdentificationNumber]
           ,[IdentificationTypeId]
           ,[GenderId]
           ,[IsActive]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           ('SUPERCEO'
           ,'vIXImzfLstHQlNPMVw7dOWv9FFpCBCyqIUm50np4Qlw='
           ,'soporte@cloudonesoft.com'
           ,'Super'
           ,'Admin'
           ,NULL
           ,NULL
           ,NULL
           ,1
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[ProfileUsers]
           ([ProfileId]
           ,[UserId]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (1
           ,1
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[Menus]
           ([Id]
           ,[Name]
           ,[ResourceKey]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (1
           ,'User'
           ,'User'
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[Menus]
           ([Id]
           ,[Name]
           ,[ResourceKey]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (2
           ,'Profile'
           ,'Profile'
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[Menus]
           ([Id]
           ,[Name]
           ,[ResourceKey]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (3
           ,'CargaArchivos'
           ,'CargaArchivos'
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[Menus]
           ([Id]
           ,[Name]
           ,[ResourceKey]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (4
           ,'Archivos'
           ,'Archivos'
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT [dbo].[MenuActions] ([ActionName], [ResourceKey], [MenuId], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES ( N'New', N'BLL.CREAR', 0, N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[MenuActions] ([ActionName], [ResourceKey], [MenuId], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES ( N'Edit', N'BLL.MODIFICAR', 0, N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[MenuActions] ([ActionName], [ResourceKey], [MenuId], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES ( N'Delete', N'BLL.BORRAR', 0, N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[MenuActions] ([ActionName], [ResourceKey], [MenuId], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES ( N'List', N'BLL.LISTAR', 0, N'admin', GETDATE(), N'admin',GETDATE())
GO


INSERT INTO [dbo].[Empresas]
           ([Id]
           ,[RazonSocial]
           ,[NumeroIdentificacion]
           ,[DV]
           ,[Direccion]
           ,[Telefono]
           ,[Celular]
           ,[CorreoElectronico]
           ,[PaginaWeb]
           ,[TiposIdentificacionRepresentanteLegalId]
           ,[NumeroIdentificacionRepresentanteLegal]
           ,[NombresApellidosRepresentanteLegal]
           ,[CIIU]
           ,[CodigoPostal]
           ,[Logo]
           ,[Alias]
           ,[CodigoReps]
           ,[Codigo85]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (1
           ,'CloudOneSoft'
           ,'901298432'
           ,'6'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'COS'
           ,NULL
           ,NULL
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

INSERT INTO [dbo].[Languages]
           ([Id]
		   ,[Code]
           ,[Culture]
           ,[Name]
           ,[Active]
           ,[UpdatedBy]
           ,[LastUpdate]
           ,[CreatedBy]
           ,[CreationDate])
     VALUES
           (2
		   ,'ESP'
           ,'es'
           ,'Español'
           ,1
           ,'admin'
           ,GETDATE()
           ,'admin'
           ,GETDATE()
		   )
GO

CREATE TABLE [dbo].[Audits](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [varchar](255) NOT NULL,
	[Action] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[KeyValues] [varchar](255) NOT NULL,
	[OldValues] [varchar](max) NULL,
	[NewValues] [varchar](max) NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Audits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

create view [dbo].[vAudits] as select * from [dbo].[Audits]
GO

CREATE TABLE [dbo].[Periodos](
	[Id] [bigint] NOT NULL,
	[Descripcion] [varchar](30) NOT NULL,
	[UpdatedBy] [varchar](50) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Periodos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ultimo usuario que actualizo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Periodos', @level2type=N'COLUMN',@level2name=N'UpdatedBy'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha ultima actualización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Periodos', @level2type=N'COLUMN',@level2name=N'LastUpdate'
GO

ALTER TABLE [dbo].[Archivos]
ADD CONSTRAINT FK_Archivos_Periodos
FOREIGN KEY (PeriodoId) REFERENCES [dbo].[Periodos]([Id]);


INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (1, N'Enero', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (2, N'Febrero', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (3, N'Marzo', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (4, N'Abril', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (5, N'Mayo', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (6, N'Junio', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (7, N'Julio', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (8, N'Agosto', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (9, N'Septiembre', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (10, N'Octubre', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (11, N'Noviembre', N'admin', GETDATE(), N'admin', GETDATE())
GO
INSERT [dbo].[Periodos] ([Id], [Descripcion], [UpdatedBy], [LastUpdate], [CreatedBy], [CreationDate]) VALUES (12, N'Diciembre', N'admin', GETDATE(), N'admin', GETDATE())
GO

