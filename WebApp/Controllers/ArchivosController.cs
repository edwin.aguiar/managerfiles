using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using DevExtreme.AspNet.Mvc;
using Dominus.Frontend.Controllers;
using Blazor.Infrastructure.Entities;
using Blazor.WebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Newtonsoft.Json;
using Blazor.BusinessLogic;

namespace Blazor.WebApp.Controllers
{

    [Authorize]
    public partial class ArchivosController : BaseAppController
    {

        //private const string Prefix = "Archivos"; 

        public ArchivosController(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {
        }

        #region Functions Master

        [HttpPost]
        public LoadResult Get(DataSourceLoadOptions loadOptions)
        {
            var result = Manager().GetBusinessLogic<Archivos>().Tabla(true).Select(x => new
            {
                Id = x.Id,
                PeriodoId = x.PeriodoId,
                Periodo = x.Periodo,
                Nombre = x.Nombre,
                PesoResumen = x.PesoResumen,
                Descripcion = x.Descripcion,
                CreationDate = x.CreationDate,
                CreatedBy = x.CreatedBy
            });
            return DataSourceLoader.Load(result, loadOptions);
        }

        public IActionResult List()
        {
            return View("List");
        }

        public IActionResult ListPartial()
        {
            return PartialView("List");
        }

        [HttpGet]
        public IActionResult New()
        {
            return PartialView("Edit", NewModel());
        }

        private ArchivosModel NewModel()
        {
            ArchivosModel model = new ArchivosModel();
            model.Entity.IsNew = true;
            return model;
        }

        [HttpGet]
        public IActionResult Edit(long Id)
        {
            return PartialView("Edit", EditModel(Id));
        }

        private ArchivosModel EditModel(long Id)
        {
            ArchivosModel model = new ArchivosModel();
            model.Entity = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == Id, false);
            model.Entity.IsNew = false;
            return model;
        }

        [HttpPost]
        public IActionResult Edit(ArchivosModel model)
        {
            return PartialView("Edit", EditModel(model));
        }

        private ArchivosModel EditModel(ArchivosModel model)
        {
            ViewBag.Accion = "Save";
            var OnState = model.Entity.IsNew;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Entity.LastUpdate = DateTime.Now;
                    model.Entity.UpdatedBy = User.Identity.Name;
                    if (model.Entity.IsNew)
                    {
                        model.Entity.CreationDate = DateTime.Now;
                        model.Entity.CreatedBy = User.Identity.Name;
                        model.Entity = Manager().GetBusinessLogic<Archivos>().Add(model.Entity);
                        model.Entity.IsNew = false;
                    }
                    else
                    {
                        model.Entity = Manager().GetBusinessLogic<Archivos>().Modify(model.Entity);
                    }
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Entity.Id", e.GetFullErrorMessage());
                }
            }
            else
            {
                ModelState.AddModelError("Entity.Id", "Error de codigo, el objeto a guardar tiene campos diferentes a los de la entidad.");
            }
            return model;
        }

        [HttpPost]
        public IActionResult Delete(ArchivosModel model)
        {
            return PartialView("Edit", DeleteModel(model));
        }

        private ArchivosModel DeleteModel(ArchivosModel model)
        {
            ViewBag.Accion = "Delete";
            ArchivosModel newModel = NewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    model.Entity = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == model.Entity.Id, false);
                    Manager().GetBusinessLogic<Archivos>().Remove(model.Entity);
                    return newModel;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Entity.Id", e.GetFullErrorMessage());
                }
            }
            return model;
        }

        #endregion

        #region Functions Detail 
        /*

        [HttpGet]
        public IActionResult NewDetail(long IdFather)
        {
            return PartialView("EditDetail", NewModelDetail(IdFather));
        }

        private ArchivosModel NewModelDetail(long IdFather) 
        { 
            ArchivosModel model = new ArchivosModel(); 
            model.Entity.IdFather = IdFather; 
            model.Entity.IsNew = true; 
            return model; 
        } 

        [HttpGet]
        public IActionResult EditDetail(long Id)
        {
            return PartialView("EditDetail", EditModel(Id));
        }

        [HttpPost]
        public IActionResult EditDetail(ArchivosModel model)
        {
            return PartialView("EditDetail",EditModel(model));
        }

        [HttpPost]
        public IActionResult DeleteDetail(ArchivosModel model)
        {
            return PartialView("EditDetail", DeleteModelDetail(model));
        }

        private ArchivosModel DeleteModelDetail(ArchivosModel model)
        { 
            ViewBag.Accion = "Delete"; 
            ArchivosModel newModel = NewModelDetail(model.Entity.IdFather); 
            if (ModelState.IsValid) 
            { 
                try 
                { 
                    model.Entity = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == model.Entity.Id, false); 
                    Manager().GetBusinessLogic<Archivos>().Remove(model.Entity); 
                    return newModel;
                } 
                catch (Exception e) 
                { 
                    ModelState.AddModelError("Entity.Id", e.GetFullErrorMessage()); 
                } 
            } 
            return model; 
        } 

        #endregion 

        #region Funcions Detail Edit in Grid 

        [HttpPost] 
        public IActionResult AddInGrid(string values) 
        { 
             Archivos entity = new Archivos(); 
             JsonConvert.PopulateObject(values, entity); 
             ArchivosModel model = new ArchivosModel(); 
             model.Entity = entity; 
             model.Entity.IsNew = true; 
             this.EditModel(model); 
             if(ModelState.IsValid) 
                 return Ok(ModelState); 
             else 
                 return BadRequest(ModelState); 
        } 

        [HttpPost] 
        public IActionResult ModifyInGrid(int key, string values) 
        { 
             Archivos entity = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == key, false); 
             JsonConvert.PopulateObject(values, entity); 
             ArchivosModel model = new ArchivosModel(); 
             model.Entity = entity; 
             model.Entity.IsNew = false; 
             this.EditModel(model); 
             if(ModelState.IsValid) 
                 return Ok(ModelState); 
             else 
                 return BadRequest(ModelState); 
        } 

        [HttpPost]
        public void DeleteInGrid(int key)
        { 
             Archivos entity = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == key, false); 
             Manager().GetBusinessLogic<Archivos>().Remove(entity); 
        } 

        */
        #endregion


        [HttpGet]
        public IActionResult DescargarArchivo(long id)
        {
            try
            {
                Archivos archivo = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == id, false);
                if (archivo != null &&
                    archivo.Archivo.Length > 0 &&
                    !string.IsNullOrWhiteSpace(archivo.TipoContenido) &&
                    !string.IsNullOrWhiteSpace(archivo.Nombre)
                    )
                {
                    string nombreArchivo = archivo.Nombre;
                    if (!archivo.Nombre.EndsWith(archivo.Extension))
                        nombreArchivo = string.Concat(archivo.Nombre, archivo.Extension);
                    return File(archivo.Archivo, archivo.TipoContenido, nombreArchivo);
                }
                else
                {
                    return new BadRequestObjectResult("Error obteniendo archivo de resultado. Verificar Base de datos.");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error en servidor. " + e.GetFullErrorMessage());
            }

        }

        [HttpPost]
        public IActionResult EliminarArchivo(long id)
        {
            try
            {
                Archivos archivo = Manager().GetBusinessLogic<Archivos>().FindById(x => x.Id == id, false);
                if (archivo != null)
                {
                    Manager().GetBusinessLogic<Archivos>().Remove(archivo);
                    return Ok();
                }
                else
                {
                    return new BadRequestObjectResult("Error eliminando archivo de resultado. Verificar Base de datos.");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error en servidor. " + e.GetFullErrorMessage());
            }
        }
    }
}
