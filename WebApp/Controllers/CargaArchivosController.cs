﻿using Blazor.BusinessLogic;
using Blazor.Infrastructure.Entities;
using Blazor.WebApp.Models;
using Dominus.Backend.Application;
using Dominus.Frontend.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Blazor.WebApp.Controllers
{
    [Authorize]
    public class CargaArchivosController : BaseAppController
    {
        public CargaArchivosController(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {
        }

        public IActionResult List()
        {
            CargaArchivosModel model = new CargaArchivosModel();
            model.Periodos = Manager().GetBusinessLogic<Periodos>().FindAll(x => true);
            return View("CargaArchivos", model);
        }

        public IActionResult ListPartial()
        {
            CargaArchivosModel model = new CargaArchivosModel();
            model.Periodos = Manager().GetBusinessLogic<Periodos>().FindAll(x => true);
            return PartialView("CargaArchivos", model);
        }

        [HttpPost]
        [RequestSizeLimit(int.MaxValue)]
        public ActionResult CargarArchivos(long periodo, string descripcion)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            try
            {
                IFormFile myFile = Request.Form.Files["CargaArchivos"];
                Archivos archivo = new Archivos();
                archivo.Nombre = myFile.FileName.Substring(myFile.FileName.Length > 1000 ? myFile.FileName.Length - 999 : 0);
                archivo.Extension = myFile.FileName.Substring(myFile.FileName.LastIndexOf("."));
                archivo.TipoContenido = myFile.ContentType;
                archivo.PeriodoId = periodo;
                archivo.PesoEnBytes = myFile.Length;
                archivo.PesoEnBytes = myFile.Length;
                archivo.PesoResumen = DApp.Util.CalcularPesoParaBytes(myFile.Length);
                archivo.Descripcion = descripcion;
                archivo.UpdatedBy = User.Identity.Name;
                archivo.CreatedBy = User.Identity.Name;
                archivo.CreationDate = DateTime.Now;
                archivo.LastUpdate = DateTime.Now;
                using (var ms = new MemoryStream())
                {
                    myFile.CopyTo(ms);
                    archivo.Archivo = ms.ToArray();
                }

                Manager().GetBusinessLogic<Archivos>().Add(archivo);

                result.Add("TieneErrores", false);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                result.Add("TieneErrores", true);
                result.Add("Errores", new List<string> { "Error al subir imagen. | " + e.GetFullErrorMessage() });
                return new BadRequestObjectResult(result);
            }
        }

    }
}
