﻿using Blazor.Infrastructure.Entities;
using Dominus.Backend.Application;
using Dominus.Backend.Data;
using Dominus.Backend.DataBase;
using System.Collections.Generic;

namespace Blazor.BusinessLogic
{
    public static class BusinessLogicExtentions
    {
        public static GenericBusinessLogic<T> GetBusinessLogic<T>(this Dominus.Backend.DataBase.BusinessLogic logic) where T : BaseEntity
        {
            if (typeof(T) == typeof(User))
                return new UserBusinessLogic(logic.settings) as GenericBusinessLogic<T>;
            
            return new GenericBusinessLogic<T>(logic.settings);

        }

        public static UserBusinessLogic UserBusinessLogic(this Dominus.Backend.DataBase.BusinessLogic logic)
        {
            return new UserBusinessLogic(logic.settings);
        }
        

        #region Identificadores estaticos

        //public static Dictionary<string, Dictionary<string, string>> GetResources(this Dominus.Backend.DataBase.BusinessLogic logic)
        //{
        //    return DApp.DefaultLanguage.GetResources();
        //}

        public static List<KeyValue> GetPeriodos(this Dominus.Backend.DataBase.BusinessLogic logic)
        {
            return new List<KeyValue>
            {
                new KeyValue { Key = 1, Id = "1", Value = "Enero" },
                new KeyValue { Key = 2, Id = "2", Value = "Febrero" },
                new KeyValue { Key = 3, Id = "3", Value = "Marzo" },
                new KeyValue { Key = 4, Id = "4", Value = "Abril" },
                new KeyValue { Key = 5, Id = "6", Value = "Mayo" },
                new KeyValue { Key = 6, Id = "6", Value = "Junio" },
                new KeyValue { Key = 7, Id = "7", Value = "Julio" },
                new KeyValue { Key = 8, Id = "8", Value = "Agosto" },
                new KeyValue { Key = 9, Id = "9", Value = "Septiembre" },
                new KeyValue { Key = 10, Id = "10", Value = "Octubre" },
                new KeyValue { Key = 11, Id = "11", Value = "Noviembre" },
                new KeyValue { Key = 12, Id = "12", Value = "Diciembre" },
            };
        }


        #endregion
    }
}