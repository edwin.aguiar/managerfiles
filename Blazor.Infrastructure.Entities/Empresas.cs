using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using Serialize.Linq.Extensions;
using Dominus.Backend.Data;
using Dominus.Backend.DataBase;

namespace Blazor.Infrastructure.Entities
{
    /// <summary>
    /// Empresas object for mapped table Empresas.
    /// </summary>
    [Table("Empresas")]
    public partial class Empresas : BaseEntity
    {

       #region Columnas normales)

       [Column("RazonSocial")]
       [DDisplayName("Empresas.RazonSocial")]
       [DRequired("Empresas.RazonSocial")]
       [DStringLength("Empresas.RazonSocial",150)]
       public virtual String RazonSocial { get; set; }

       [Column("NumeroIdentificacion")]
       [DDisplayName("Empresas.NumeroIdentificacion")]
       [DRequired("Empresas.NumeroIdentificacion")]
       [DStringLength("Empresas.NumeroIdentificacion",45)]
       public virtual String NumeroIdentificacion { get; set; }

       [Column("DV")]
       [DDisplayName("Empresas.DV")]
       [DRequired("Empresas.DV")]
       [DStringLength("Empresas.DV",2)]
       public virtual String DV { get; set; }

       [Column("Direccion")]
       [DDisplayName("Empresas.Direccion")]
       [DStringLength("Empresas.Direccion",200)]
       public virtual String Direccion { get; set; }

       [Column("Telefono")]
       [DDisplayName("Empresas.Telefono")]
       [DStringLength("Empresas.Telefono",10)]
       public virtual String Telefono { get; set; }

       [Column("Celular")]
       [DDisplayName("Empresas.Celular")]
       [DStringLength("Empresas.Celular",10)]
       public virtual String Celular { get; set; }

       [Column("CorreoElectronico")]
       [DDisplayName("Empresas.CorreoElectronico")]
       [DStringLength("Empresas.CorreoElectronico",200)]
       public virtual String CorreoElectronico { get; set; }

       [Column("PaginaWeb")]
       [DDisplayName("Empresas.PaginaWeb")]
       [DStringLength("Empresas.PaginaWeb",100)]
       public virtual String PaginaWeb { get; set; }

       [Column("TiposIdentificacionRepresentanteLegalId")]
       [DDisplayName("Empresas.TiposIdentificacionRepresentanteLegalId")]
       public virtual Int64? TiposIdentificacionRepresentanteLegalId { get; set; }

       [Column("NumeroIdentificacionRepresentanteLegal")]
       [DDisplayName("Empresas.NumeroIdentificacionRepresentanteLegal")]
       [DStringLength("Empresas.NumeroIdentificacionRepresentanteLegal",45)]
       public virtual String NumeroIdentificacionRepresentanteLegal { get; set; }

       [Column("NombresApellidosRepresentanteLegal")]
       [DDisplayName("Empresas.NombresApellidosRepresentanteLegal")]
       [DStringLength("Empresas.NombresApellidosRepresentanteLegal",100)]
       public virtual String NombresApellidosRepresentanteLegal { get; set; }

       [Column("CIIU")]
       [DDisplayName("Empresas.CIIU")]
       [DStringLength("Empresas.CIIU",20)]
       public virtual String CIIU { get; set; }

       [Column("CodigoPostal")]
       [DDisplayName("Empresas.CodigoPostal")]
       [DStringLength("Empresas.CodigoPostal",20)]
       public virtual String CodigoPostal { get; set; }

       [Column("Logo")]
       [DDisplayName("Empresas.Logo")]
       public virtual Byte[] Logo { get; set; }

       [Column("Alias")]
       [DDisplayName("Empresas.Alias")]
       [DRequired("Empresas.Alias")]
       [DStringLength("Empresas.Alias",50)]
       public virtual String Alias { get; set; }

       [Column("CodigoReps")]
       [DDisplayName("Empresas.CodigoReps")]
       [DStringLength("Empresas.CodigoReps",45)]
       public virtual String CodigoReps { get; set; }

       [Column("Codigo85")]
       [DDisplayName("Empresas.Codigo85")]
       [DStringLength("Empresas.Codigo85",2)]
       public virtual String Codigo85 { get; set; }

       #endregion

       #region Reglas expression

       public override Expression<Func<T, bool>> PrimaryKeyExpression<T>()
       {
       Expression<Func<Empresas, bool>> expression = entity => entity.Id == this.Id;
       return expression as Expression<Func<T, bool>>;
       }

       public override List<ExpRecurso> GetAdicionarExpression<T>()
       {
        var rules = new List<ExpRecurso>();
        Expression<Func<Empresas, bool>> expression = null;

        expression = entity => entity.Alias == this.Alias;
        rules.Add(new ExpRecurso(expression.ToExpressionNode() , new Recurso("BLL.BUSINESS.UNIQUE", "Empresas.Alias" )));

       return rules;
       }

       public override List<ExpRecurso> GetModificarExpression<T>()
       {
        var rules = new List<ExpRecurso>();
        Expression<Func<Empresas, bool>> expression = null;

        expression = entity => !(entity.Id == this.Id && entity.Alias == this.Alias)
                               && entity.Alias == this.Alias;
        rules.Add(new ExpRecurso(expression.ToExpressionNode() , new Recurso("BLL.BUSINESS.UNIQUE", "Empresas.Alias" )));

       return rules;
       }

       public override List<ExpRecurso> GetEliminarExpression<T>()
       {
        var rules = new List<ExpRecurso>();
       return rules;
       }

       #endregion
    }
 }
