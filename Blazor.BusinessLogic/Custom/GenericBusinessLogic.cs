﻿using Blazor.Infrastructure;
using Blazor.Infrastructure.Entities;
using Dominus.Backend.Application;
using Dominus.Backend.DataBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static Dominus.Backend.Application.Language;
using LanguageResource = Dominus.Backend.Application.Language.LanguageResource;

namespace Blazor.BusinessLogic
{
    public class GenericBusinessLogic<T> : IDomainLogic<T> where T : BaseEntity
    {
        public GenericBusinessLogic(IUnitOfWork unitWork)
        {
            UnitOfWork = unitWork;
            CommitTheTransaction = false;
        }

        public GenericBusinessLogic(DataBaseSetting configuracionBD)
        {
            UnitOfWork = new BlazorUnitWork(configuracionBD);
            CommitTheTransaction = true;
        }

    }
}
