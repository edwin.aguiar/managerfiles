﻿using Blazor.Infrastructure.Entities;
using Dominus.Backend.DataBase;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace Blazor.Infrastructure
{
    public class BlazorContext : DContext
    {
        public DbSet<Archivos> Archivos { get; set; }
        public DbSet<Audit> Audit { get; set; }
        public DbSet<Empresas> Empresas { get; set; }
        public DbSet<MenuAction> MenuAction { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<ProfileNavigation> ProfileNavigation { get; set; }
        public DbSet<Profile> Profile { get; set; }
        public DbSet<ProfileUser> ProfileUser { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<LanguageResource> LanguageResource { get; set; }
        public DbSet<Languages> Languages { get; set; }

        public BlazorContext(DataBaseSetting setting) : base(setting)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if DEBUG
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
#endif
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

        }
    }
}